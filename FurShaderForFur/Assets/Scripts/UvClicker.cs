﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UvClicker : MonoBehaviour {

    public Camera cam;
    public float radius = 5.0f;
    private List<Vector2> pixels;

    public bool resetBrush = false;
    void Start () {
        cam = GetComponent<Camera>();//Camera.main;
    }
	
	// Update is called once per frame
	void Update () {
        if (!Input.GetMouseButton(0))
            return;

        RaycastHit hit;
        if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
            return;

        Renderer rend = hit.transform.GetComponent<Renderer>();
        MeshCollider meshCollider = hit.collider as MeshCollider;

        if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
            return;

        Texture2D tex = rend.material.mainTexture as Texture2D;
        Vector2 pixelUV = hit.textureCoord;
        pixelUV.x *= tex.width;
        pixelUV.y *= tex.height;

        Debug.Log(pixelUV);

        Color test = new Color();
        test.r = pixelUV.x / tex.width;
        test.g = pixelUV.y / tex.height;
        test.b = radius;

        if (resetBrush)
        {
            test = new Color(0.0f, 0.0f, 0.0f, 0.0f);
        }

        int px, nx, py, ny, d;
        int cx = (int)pixelUV.x;
        int cy = (int)pixelUV.y;
        for(int i = 0; i < radius; i++)
        {
            d = (int)Mathf.Ceil(Mathf.Sqrt(radius * radius - i * i));
            for(int j = 0; j < d; j++)
            {
                px = cx + i;
                nx = cx - i;
                py = cy + j;
                ny = cy - j;


                tex.SetPixel(px, py, test);
                tex.SetPixel(nx, py, test);

                tex.SetPixel(px, ny, test);
                tex.SetPixel(nx, ny, test);
            }
        }
        tex.Apply();
    }
}
