﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LODdemo : MonoBehaviour {

    // Use this for initialization
    private float m_Timer = 0.0f;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        m_Timer += Time.deltaTime;
        if(m_Timer >= 1.0f)
        {
            m_Timer = 0.0f;

            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("fur"))
            {
                obj.GetComponent<LODscript>().UpdateLOD();
            }
        }
	}
}
