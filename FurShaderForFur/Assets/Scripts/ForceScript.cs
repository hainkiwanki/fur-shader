﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ForceScript : MonoBehaviour {

    public bool m_UseWind = false;
    public Vector3 m_WindDirection = new Vector3(0,0,0);

    private bool m_useGravity;
    private float gravityMultiplier = 0.05f;
    private Renderer m_Renderer;
    private Rigidbody m_RigidBody;
    private Vector3 m_Force, m_LerpForce;

    Quaternion prv;
    Vector3 angle, tangle;
    Vector3 angleP, angleMov;

    [Range(0.0f, 1.0f)]
    public float angularMultiplier = 0.2f;
    [Range(0.0f, 1.0f)]
    public float windMultiplier = 0.0f;
    [Range(0.2f, 0.5f)]
    public float forceMultiplier = 0.3f;
    [Range(1.0f, 100.0f)]
    public float dampFactor = 10.0f;
	void Start () {
        m_Renderer = GetComponent<Renderer>();
        m_RigidBody = GetComponent<Rigidbody>();
        if(m_RigidBody == null)
        {
            gameObject.AddComponent<Rigidbody>();
        }
        m_LerpForce = Vector3.zero;
        m_useGravity = m_RigidBody.useGravity;
        prv = transform.rotation;
    }

	void Update () {
        //Moving velocity
        m_Force = -m_RigidBody.velocity * forceMultiplier;

        //Gravity
        m_useGravity = m_RigidBody.useGravity;
        if (!m_useGravity)
        {
            m_RigidBody.velocity = Vector3.zero;
        }
        else
        {
            m_Force += Physics.gravity * gravityMultiplier;
        }

        //Wind
        if (m_UseWind)
        {
            m_Force += m_WindDirection * windMultiplier;
            m_Renderer.sharedMaterial.SetFloat("_UseWind", 1.0f);
        }
        else
        {
            m_Renderer.sharedMaterial.SetFloat("_UseWind", 0.0f);
        }

        //Smoothing the transition
        m_LerpForce = Vector3.Lerp(m_LerpForce, m_Force, Time.deltaTime * dampFactor);

        //Setting variable in shader
        m_Renderer.sharedMaterial.SetVector("_Force", m_LerpForce);

    }
}
