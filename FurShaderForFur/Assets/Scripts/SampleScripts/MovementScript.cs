﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {
    public float m_Speed = 10.0f;

    private float m_Time;
    private Rigidbody m_Rigid;
    private Vector3 m_Vel;
    private Vector3 m_CurVel;
	void Start () {
        m_Rigid = GetComponent<Rigidbody>();
	}
	void Update () {

        m_Vel = Vector3.zero;
        if (Input.GetKey(KeyCode.J))
        {
            m_Vel.x = -1 * m_Speed;
        }
        else if (Input.GetKey(KeyCode.L))
        {
            m_Vel.x = m_Speed;
        }
        if (Input.GetKey(KeyCode.I))
        {
            m_Vel.y = m_Speed;
        }
        else if (Input.GetKey(KeyCode.K))
        {
            m_Vel.y = -1 * m_Speed;
        }

        m_Rigid.MovePosition(gameObject.transform.position + m_Vel * Time.deltaTime);
        m_Rigid.velocity = m_Vel;
	}
}
