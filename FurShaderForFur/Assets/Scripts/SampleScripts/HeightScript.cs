﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightScript : MonoBehaviour {
    private Renderer m_Rend;
    private float m_Timer;
    public List<Texture> m_Textures = new List<Texture>();
    private int m_Counter = 0;
    void Start()
    {
        m_Rend = GetComponent<Renderer>();
    }
    void Update()
    {
        m_Timer += Time.deltaTime;
        if(m_Timer >= 4.0f)
        {
            m_Counter++;
            if (m_Counter == 5)
                m_Counter = 0;
            m_Rend.sharedMaterial.SetTexture("_HairControlMap", m_Textures[m_Counter]);
            m_Timer = 0.0f;
        }
    }
}
