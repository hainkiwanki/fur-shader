﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThinnessScript : MonoBehaviour {

    private Renderer m_Rend;
    private float m_Timer = 0.0f;
    private float m_Thinness;
    void Start()
    {
        m_Rend = GetComponent<Renderer>();
    }
    void Update()
    {
        m_Timer += Time.deltaTime;
        m_Thinness = Mathf.Lerp(0.1f, 5.0f, m_Timer / 10.0f);
        m_Rend.sharedMaterial.SetFloat("_FurThinness", m_Thinness);
        if (m_Timer >= 10.0f)
        {
            m_Timer = 0.0f;
        }
    }
}
