﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StiffnessScript : MonoBehaviour {

    private Renderer m_Rend;
    private float m_Timer = 0.0f;
    private bool m_bDir = true;
	void Start () {
        m_Rend = GetComponent<Renderer>();
	}
	void Update () {
        m_Timer += Time.deltaTime;
        if(m_Timer >= 9.0f)
        {
            m_Timer = 0.0f;
            m_bDir = !m_bDir;
        }
        if (m_bDir)
        {
            m_Rend.sharedMaterial.SetFloat("_Stiffness", m_Timer / 3.0f);
        }
        else
        {
            m_Rend.sharedMaterial.SetFloat("_Stiffness", (9.0f - m_Timer) / 3.0f);
        }
	}
}
