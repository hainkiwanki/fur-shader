﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorScript : MonoBehaviour {
    private Renderer m_Rend;
    private float m_Timer;
    void Start()
    {
        m_Rend = GetComponent<Renderer>();
    }
    void Update()
    {
        m_Timer += Time.deltaTime;
        m_Rend.sharedMaterial.SetFloat("_MinColor", Mathf.Sin(m_Timer));
    }
}
