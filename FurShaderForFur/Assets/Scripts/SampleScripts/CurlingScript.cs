﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurlingScript : MonoBehaviour {
    private Renderer m_Rend;
    private float m_Timer;
    private float m_Multi = 0.0f;
    void Start()
    {
        m_Rend = GetComponent<Renderer>();
    }
    void Update()
    {
        m_Timer += Time.deltaTime;
        if (m_Timer >= 3.0f)
        {
            m_Multi += 5.0f;
            if (m_Multi >= 20.0f)
                m_Multi = 5.0f;
            m_Rend.sharedMaterial.SetFloat("_CurlFrequency", m_Multi);
            m_Timer = 0.0f;
        }
    }
}
