﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LengthScript : MonoBehaviour {

    private Renderer m_Rend;
    private float m_Timer = 0.0f;
    private float m_Length;
    void Start()
    {
        m_Rend = GetComponent<Renderer>();
    }
    void Update()
    {
        m_Timer += Time.deltaTime;
        m_Length = Mathf.Lerp(1.0f, 60.0f, m_Timer / 5.0f);
        m_Rend.sharedMaterial.SetFloat("_MaxFurLength", m_Length);
        if(m_Timer >= 5.0f)
        {
            m_Timer = 0.0f;
        }
    }
}
