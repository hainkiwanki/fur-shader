﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LODscript : MonoBehaviour {

    private float m_Distance;
    private Shader[] m_Shaders;
    public List<float> m_Distances = new List<float>();
    private int m_LODlevel = 0;
    private Renderer m_Rend;
    private Camera m_Cam;
	void Start () {
        m_Rend = GetComponent<Renderer>();
        m_Shaders = new Shader[7];
        m_Shaders[0] = Shader.Find("GW/Shells/1_Layer");
        m_Shaders[1] = Shader.Find("GW/Shells/16_Layers");
        m_Shaders[2] = Shader.Find("GW/Shells/24_Layers");
        m_Shaders[3] = Shader.Find("GW/Shells/32_Layers");
        m_Shaders[4] = Shader.Find("GW/Shells/40_Layers");
        m_Shaders[5] = Shader.Find("GW/Shells/48_Layers");
        m_Shaders[6] = Shader.Find("GW/Shells/64_Layers");

        m_Cam = Camera.main;

        m_LODlevel = -1;
    }
	
	// Update is called once per frame
	public void UpdateLOD () {
        m_Distance = Vector3.Distance(m_Cam.transform.position, gameObject.transform.position);

        if(m_Distance > m_Distances[0])
        {
            if(m_LODlevel != 0)
            {
                m_LODlevel = 0;
                m_Rend.material.shader = m_Shaders[0];
            }
        }
        else if(m_Distance > m_Distances[1])
        {
            if(m_LODlevel != 1)
            {
                m_LODlevel = 1;
                m_Rend.material.shader = m_Shaders[1];
            }
        }
        else if(m_Distance > m_Distances[2])
        {
            if(m_LODlevel != 2)
            {
                m_LODlevel = 2;
                m_Rend.material.shader = m_Shaders[2];
            }
        }
        else if (m_Distance > m_Distances[3])
        {
            if (m_LODlevel != 3)
            {
                m_LODlevel = 3;
                m_Rend.material.shader = m_Shaders[3];
            }
        }
        else if (m_Distance > m_Distances[4])
        {
            if (m_LODlevel != 4)
            {
                m_LODlevel = 4;
                m_Rend.material.shader = m_Shaders[4];
            }
        }
        else if (m_Distance > m_Distances[5])
        {
            if (m_LODlevel != 5)
            {
                m_LODlevel = 5;
                m_Rend.material.shader = m_Shaders[5];
            }
        }
        else if (m_LODlevel != 6)
        {
            m_LODlevel = 6;
            m_Rend.material.shader = m_Shaders[6];
        }
    }
}
