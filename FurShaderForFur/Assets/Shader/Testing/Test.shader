﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Test" {
	Properties{
		_MainTex("Texture", 2D) = "white"
	}
		SubShader{
			Cull Front
			Pass{
				CGPROGRAM
				#pragma vertex vert
				#pragma geometry geom
				#pragma fragment frag
				#include "UnityCG.cginc"

				sampler2D _MainTex;

				struct VS_DATA {
					float4 Position: POSITION;
					float3 Normal: NORMAL;
					float2 TexCoord: TEXCOORD;
				};

				struct GS_DATA {
					float4 Position : SV_POSITION;
					float3 Normal : NORMAL;
					float2 TexCoord : TEXCOORD0;
				};

				VS_DATA vert(VS_DATA dat) {
					return dat;
				}

				void CreateVertex(inout TriangleStream<GS_DATA> triStream, float3 pos, float3 normal, float2 texCoord)
				{
					GS_DATA gsData = (GS_DATA)0;
					//Step 2. Transform the position using the WVP Matrix
					// mul(float4(pos, 1), UNITY_MATRIX_MVP);
					gsData.Position = UnityObjectToClipPos(float4(pos, 1));
					//Step 3. Transform the normal using the World Matrix
					gsData.Normal = mul(normal, (float3x3)unity_ObjectToWorld);
					//Step 4. Assign texCoord to (GS_DATA object).TexCoord
					gsData.TexCoord = texCoord;
					//Step 5. Append (GS_DATA object) to the TriangleStream parameter (TriangleStream::Append(...))
					triStream.Append(gsData);
				}

				[maxvertexcount(6)]
				void geom(triangle VS_DATA vertices[3], inout TriangleStream<GS_DATA> triStream) {

					//Use these variable names
					float3 basePoint, top, left, right, spikeNormal;

					basePoint = (vertices[0].Position + vertices[1].Position + vertices[2].Position) / 3;
					float3 basePointNormal = (vertices[0].Normal + vertices[1].Normal + vertices[2].Normal) / 3;
					top = basePoint + basePointNormal * 0.5f;
					float3 dir = (vertices[2].Position - vertices[0].Position) * 0.1f;
					left = basePoint - dir;
					right = basePoint + dir;
					spikeNormal = cross(top - left, top - right);


					CreateVertex(triStream, vertices[0].Position, vertices[0].Normal, vertices[0].TexCoord);
					CreateVertex(triStream, vertices[1].Position, vertices[1].Normal, vertices[1].TexCoord);
					CreateVertex(triStream, vertices[2].Position, vertices[2].Normal, vertices[2].TexCoord);

					triStream.RestartStrip();

					CreateVertex(triStream, top, spikeNormal, float2(0, 0));
					CreateVertex(triStream, left, spikeNormal, float2(0, 0));
					CreateVertex(triStream, right, spikeNormal, float2(0, 0));
				}

				float4 frag(GS_DATA input) : SV_TARGET
				{
					input.Normal = -normalize(input.Normal);
					float alpha = tex2D(_MainTex, input.TexCoord).a; //m_TextureDiffuse.Sample(samLinear,input.TexCoord).a;
					float3 color = tex2D(_MainTex, input.TexCoord).rgb;
					float s = max(dot(_WorldSpaceLightPos0,input.Normal), 0.4f);

					return float4(color*s,alpha);
				}

				ENDCG
			}
	}
}