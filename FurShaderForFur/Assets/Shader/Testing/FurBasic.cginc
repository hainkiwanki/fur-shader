struct Input
{
    float2 uv_MainTex;
};

float _MaxFurLength;
float3 _Force;
float3 _AngularForce;
float _Angle;
sampler2D _MainTex;
sampler2D _HairDensity;
sampler2D _HeightMap;
sampler2D _Wetness;
int _Power;
float4 _Color;
float4 _wetColor;
float _Shininess;
float _Gloss;
float _UvMul;
float _wFactor;
float _curlFactor;
float _curlSize;
float _curlRand;
float _MinColor;
float _MaxColor;
float _UseWind;
float3 _WindForce;
float _ClumpMult;
float _WindAmp;
float _WindMultiplier;

float rand(float n)
{
    return frac(sin(n) * 43758.5453123);
}

void vertMain(inout appdata_full input, out Input output)
{
    UNITY_INITIALIZE_OUTPUT(Input, output);
}

void surfMain(Input input, inout SurfaceOutput output)
{
    output.Albedo = tex2D(_MainTex, input.uv_MainTex) * _Color * 0.4f;
    output.Specular = 1.0f;
    output.Gloss = 0.0f;
}

float lengthOfVector(float3 vec)
{
    return sqrt(pow(vec.x, 2) + pow(vec.y, 2) + pow(vec.z, 2));
}

void vertShell(inout appdata_full input, out Input output)
{
    UNITY_INITIALIZE_OUTPUT(Input, output);

    //Vertex positions
    float3 p = input.vertex.xyz;

    //Length of hair
    float length = CURRENTLAYER * (_MaxFurLength / 20);
    float movementFactor = pow(CURRENTLAYER, _Power);

    //Rotation
    /*float3 v = mul(unity_WorldToObject, float4(_AngularForce.xyz, 1)).xyz;
    float dotP = dot(p, v);
    float3 v2 = v * dotP;
    float thetha = acos(dot(v2, p) / (lengthOfVector(v2) * lengthOfVector(p)));
    float r = lengthOfVector(p) * sin(thetha);
    float3 addedForce = _AngularForce.xyz * r;
    float magnitude = lengthOfVector(addedForce);
    float3 crossV = cross(float3(0, sign(_AngularForce.y) * -1, 0), input.normal) * magnitude;
    float s = sin(_Angle);
    float c = cos(_Angle);
    float3 u = normalize(_AngularForce.xyz);
    float oc = 1 - c;
    float3x3 rotMatrix = float3x3
    (   c + u.x * u.x * oc,         u.x * u.y * oc - u.z * s,   u.x * u.z * oc + u.y * s,
        u.y * u.x * oc + u.z * s,   c + u.y * u.y * oc,         u.y * u.z * oc - u.x * s,
        u.z * u.x * oc - u.y * s,   u.z * u.y * oc + u.x * s,   c + u.z * u.z * oc);*/

    //Gravity
    float3 gravity = mul(unity_WorldToObject, clamp(_Force.xyz, -1, 1)).xyz;

    //Wind
    float3 windOffset = float3(0.0f, 0.0f, 0.0f);
    if (_UseWind > 0.5f)
    {
        float phase = _Time * _WindAmp;
        float offset = (input.vertex.x + (input.vertex.z * 0.2)) * 0.5f;
        windOffset = float3(0, sin(phase + offset), 0);

    }
    windOffset = mul(unity_WorldToObject, windOffset.xyz).xyz;


    //Curling of hair
    float3 W = input.normal;
    float3 U = cross(W, float3(1, 0, 0));
    float3 V = cross(W, U);
    float b = 0.5f;
    float rot = CURRENTLAYER;
    float rot2 = CURRENTLAYER;
    if (_curlRand <= 0.5f)
    {
        rot += rand(input.vertex.x);
        rot2 += rand(input.vertex.y);
    }
    float3 normal = _curlSize * cos(rot * _curlFactor) * U + _curlSize * sin(rot2 * _curlFactor) * V + _curlSize * b * W;
    
    //Extrusion
    input.vertex.xyz += (normalize(normal + input.normal) + ((gravity) * movementFactor)) * length;
}

void surfShell(Input input, inout SurfaceOutput output)
{
    float3 noise = tex2D(_HairDensity, input.uv_MainTex * _UvMul).rgb;
    float a = noise.y;

    float3 diffuse;
    float4 wetness = tex2D(_Wetness, input.uv_MainTex);
    float height = tex2D(_HeightMap, input.uv_MainTex).r;
    float2 pixel = input.uv_MainTex;
    float alpha = 0.0f;

    if (wetness.b > 0.0f)
    {
        float2 d = (((1 - _wFactor) * CURRENTLAYER) + (_wFactor * (1 + (pow(-log(40), -1) * log(CURRENTLAYER))))) * (input.uv_MainTex - wetness.rg).xy * _ClumpMult * wetness.b;

        pixel += d;
        pixel = clamp(pixel, 0.0f, 1.0f);

        if (a >= 0.2f)
        {
            float wetness1 = tex2D(_Wetness, pixel).b;
            if(wetness1 <= 0.001f || height <= CURRENTLAYER)
                alpha = 0.0f;
            else
                alpha = 1.0f - clamp(CURRENTLAYER + (10.0 / 20.0), 0.3f, 1.0f);
        }
        diffuse = _wetColor.rgb * clamp(CURRENTLAYER, 0.5f, 1.0f);
    }
    else
    {
        diffuse = float3(1.0f, 1.0f, 1.0f);
        if (height <= CURRENTLAYER)
        {
            alpha = 0.0f;
        }
         if (a >= 0.2f)
        {
            alpha = 1.0f - clamp(CURRENTLAYER + (10.0 / 20.0), 0.3f, 1.0f);
        }
    }
    float min = clamp(noise.b, _MinColor, _MaxColor);
    float max = clamp(noise.b, min + 0.1f, 1.0f);
    diffuse *= tex2D(_MainTex, pixel).rgb * _Color * lerp(min, max, CURRENTLAYER);
    output.Albedo = diffuse.rgb;
    output.Alpha = alpha;


    output.Emission = fixed3(0.0f, 0.0f, 0.0f);
    output.Specular = 0.0f;
    output.Gloss = 0.0f;
}