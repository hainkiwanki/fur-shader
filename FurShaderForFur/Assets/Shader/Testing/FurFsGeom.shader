﻿Shader "Unlit/FurFsGeom"{
	Properties{
		_DiffuseTex("Diffuse Texture", 2D) = "white" {}
		_AmountOfLayers("Layer Amount", Int) = 5
		_MaxFurLength("Max Fur Length", Float) = 2.0
		_HairDensityTex("Hair Density Texture", 2D) = "white"{}
		_UvTiling("Texture Tiling", Float) = 1.0
		_FurLengthNoise("Fur Length Texture", 2D) = "white"{}
		_FinTexture("Fin Texture", 2D) = "white"{}
		_DotProduct("Dot Product", Float) = 0.25
		_InterShadowTexture("Inter Shadow Texture", 2D) = "black"{}
		_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
	}
	SubShader{
		Tags { "Queue" = "Transparent" "RenderType" = "Opaque" "IgnoreProjector" = "True"}

		//Generate original mesh
		Pass{
			Cull Off
			CGPROGRAM
				#pragma vertex main_vs
				#pragma fragment main_ps
				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "FurHelpers.cginc"

				sampler2D _DiffuseTex;

				gs_data main_vs(vs_data input) {
					gs_data output = (gs_data)0;

					output.pos = UnityObjectToClipPos(input.pos);
					output.norm = normalize(mul(input.norm, unity_ObjectToWorld));
					output.texc = input.texc;
					return output;
				}

				float4 main_ps(gs_data input) : SV_TARGET{
					float3 diffuse = tex2D(_DiffuseTex, input.texc);
					float NdotL = max(0.0f, dot(input.norm, normalize(input.norm - _WorldSpaceLightPos0)));
					float hLambert = NdotL * 0.5 + 0.5;
					float3 LambertDiffuse = (NdotL * diffuse);
					float3 ldiffuse =  LambertDiffuse * 1.0f * _LightColor0;
					return float4(ldiffuse, 1);
				}

			ENDCG
		}
		//Generate fins
		Pass{
			Cull Off
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite On
			CGPROGRAM
				#pragma vertex main_vs
				#pragma geometry main_gs
				#pragma fragment main_ps
				#include "UnityCG.cginc"
				#include "Lighting.cginc"
				#include "FurHelpers.cginc"
		
				float _MaxFurLength;
				Texture2D _FinTexture;
				SamplerState SmpClampPoint;
				Texture2D _DiffuseTex;
				SamplerState sampler_point_repeat;
				SamplerState sampler_linear_repeat;
				float _DotProduct;
		
				void CreateVertex(inout TriangleStream<gs_data> triStream, float4 pos, float3 normal, float2 texCoordFin, float2 texCoordDiffuse)
				{
					gs_data gsData = (gs_data)0;
					gsData.pos = UnityObjectToClipPos(mul(pos, unity_WorldToObject));
					gsData.norm = mul(normal, (float3x3)unity_WorldToObject);
					gsData.texc = texCoordFin;
					gsData.texc2 = texCoordDiffuse;
					triStream.Append(gsData);
				}
		
				gs_data main_vs(vs_data input) {
					gs_data output = (gs_data)0;
					output.pos = mul(unity_ObjectToWorld, input.pos);
					output.norm = normalize(mul(input.norm, unity_ObjectToWorld));
					output.texc = input.texc;
					output.texc2 = input.texc;
					return output;
				}
		
				[maxvertexcount(4)]
				void main_gs(line gs_data vertices[2], inout TriangleStream<gs_data> triStream) {
					float3 lineNormal = normalize((vertices[0].norm + vertices[1].norm) / 2.0f);
		
					float4 pos[4];
					pos[0] = vertices[0].pos;
					pos[1] = vertices[1].pos;
					pos[2] = pos[0] + float4((lineNormal / 15.0f * _MaxFurLength),1);
					pos[3] = pos[1] + float4((lineNormal / 15.0f * _MaxFurLength),1);
		
					float3 pointMiddle = (vertices[0].pos + vertices[1].pos) / 2.0f;
					//float3 viewDir = WorldSpaceViewDir(float4(pointMiddle, 1));
					float3 viewDir = _WorldSpaceCameraPos.xyz - mul(unity_ObjectToWorld, pointMiddle).xyz;
					float dotProduct = dot(lineNormal, viewDir);
		
					if (dotProduct <= 0.0f && dotProduct < -_DotProduct) {
						CreateVertex(triStream, pos[0], lineNormal, float2(0.0f, 0.0f), vertices[0].texc);
						CreateVertex(triStream, pos[1], lineNormal, float2(1.0f, 0.0f), vertices[1].texc);
						CreateVertex(triStream, pos[2], lineNormal, float2(0.0f, 1.0f), vertices[0].texc);
						CreateVertex(triStream, pos[3], lineNormal, float2(1.0f, 1.0f), vertices[1].texc);
		
						triStream.RestartStrip();
					}
				}
		
				float4 main_ps(gs_data input) : SV_TARGET {
					float3 diffuse = _DiffuseTex.Sample(sampler_linear_repeat, input.texc2).rgb;
					float alpha = clamp(_FinTexture.Sample(SmpClampPoint, input.texc).r - 0.6f, 0.0f, 1.0f);
					return float4(diffuse, alpha);
				}
			ENDCG
		}

		//Generate shells
		Pass{
			Cull Off
			Blend SrcAlpha OneMinusSrcAlpha
			//Blend Off
			ZWrite On

			CGPROGRAM
			#pragma vertex main_vs
			#pragma geometry main_gs
			#pragma fragment main_ps
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "FurHelpers.cginc"
		
			Texture2D _DiffuseTex;
			SamplerState sampler_point_repeat;
			SamplerState sampler_linear_repeat;
			sampler2D _HairDensityTex;
			sampler2D _FurLengthNoise;
			float _MaxFurLength;
			int _AmountOfLayers;
			float _UvTiling;
			float _ChangeLighting;

			void CreateVertex(inout TriangleStream<gs_data_shell> triStream, float4 pos, float3 normal, float2 texCoord, int layer)
			{
				gs_data_shell gsData = (gs_data_shell)0;
				gsData.pos = UnityObjectToClipPos(mul(pos, unity_WorldToObject));
				gsData.norm = mul(normal, (float3x3)unity_ObjectToWorld);
				gsData.texc = texCoord;
				gsData.layer = layer;
				triStream.Append(gsData);
			}
		
			gs_data_shell main_vs(vs_data input) {
				gs_data_shell output = (gs_data_shell)0;
				output.pos = mul(unity_ObjectToWorld, input.pos);
				output.norm = normalize(mul(input.norm, (float3x3)unity_ObjectToWorld));
				output.texc = input.texc;
				output.layer = 0;
				return output;
			}
		
			[maxvertexcount(33*3)]
			void main_gs(triangle gs_data_shell vertices[3], inout TriangleStream<gs_data_shell> triStream) {
				float maxLength = _MaxFurLength / 10.0;
				float furLength = maxLength / _AmountOfLayers;

				[loop]
				for (int i = 0; i < _AmountOfLayers; i++) {
					for (int j = 0; j < 3; j++) {
						vertices[j].pos += float4(vertices[j].norm,1) * furLength;
					}
					CreateVertex(triStream, vertices[0].pos, vertices[0].norm, vertices[0].texc, i);
					CreateVertex(triStream, vertices[1].pos, vertices[1].norm, vertices[1].texc, i);
					CreateVertex(triStream, vertices[2].pos, vertices[2].norm, vertices[2].texc, i);
				}
		
			}
		
			float4 main_ps(gs_data_shell input) : SV_TARGET {
				float3 viewDirection = WorldSpaceViewDir(input.pos);
				float3 diffuse = _DiffuseTex.Sample(sampler_linear_repeat, input.texc);
				float3 hairDens = tex2D(_HairDensityTex, input.texc * _UvTiling);
				float alpha = clamp(float(int(hairDens.r + 0.5f)), 0.0f, 1.0f);
				clip(alpha < 0.1f ? -1 : 1);
				
				float NdotL = max(0.0f, dot(input.norm, _WorldSpaceLightPos0));
				fixed4 c;
				c.rgb = diffuse * _LightColor0 * (NdotL * 0.5 + 0.5);
				c.a = 1.0f - ((float)input.layer / _AmountOfLayers);
				return c;
			}
			ENDCG
		}
	}
}