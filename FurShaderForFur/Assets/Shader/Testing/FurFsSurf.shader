﻿Shader "Custom/FurFsSurf" {
	Properties {
		_MainTex ("Fur Pattern", 2D) = "white" {}
		_HairDensity ("Fur Density Noise", 2D) = "white" {}
		_MaxFurLength ("Max Fur Length", Float) = 2.0
		_Power("Movement factor", Int) = 1
		_Color("Diffuse Color", Color) = (1,1,1,1)
		_MinColor("Minimum Color Variety", Range(0.0, 0.9)) = 0.7
		_MaxColor("Maximum Color Variety", Range(0.0, 0.9)) = 0.9
		_UvMul("Thinness", Range(0.0, 10.0)) = 1.0
		_Wetness("Wetness Texture", 2D) = "black" {}
		_wFactor("W Factor", Range(-2.0, 0.0)) = 0.0
		_curlFactor("Curls", Float) = 0.0
		_curlSize("Curl Size", Float) = 1.0
		_curlRand("Randomize Curls", Range(0.0, 1.0)) = 0.0
		_wetColor("Wet Color", Color) = (0.6, 0.6, 0.6, 1)
		_ClumpMult("Clump Multiplier", Float) = 4.0
		_WindAmp("Wind Amp X", Range(0.0, 200.0)) = 1.00
		_WindMultiplier("Wind Multi", Range(0.0, 5.0)) = 1.0
		_HeightMap("Height Map", 2D) = "white" {}
	}
	SubShader{
		Tags { "Queue" = "Transparent" "RenderType" = "Opaque" "IgnoreProjector" = "True"}
		ZWrite On
		Blend Off
		CGPROGRAM
			#pragma surface surfMain BlinnPhong addshadow vertex:vertMain
			#define CURRENTLAYER 0.0
			#include "FurBasic.cginc"
		ENDCG

		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 1.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 2.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 3.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 4.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 5.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 6.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 7.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 8.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 9.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 10.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 11.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 12.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 13.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 14.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 15.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 16.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 17.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 18.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 19.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 20.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 21.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 22.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 23.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 24.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 25.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 26.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 27.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 28.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 29.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 30.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 31 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 32.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 33.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 34.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 35.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 36.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 37.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 38.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 39.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
		
		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define CURRENTLAYER 40.0 / 40.0
			#include "FurBasic.cginc"
		ENDCG
	}
	FallBack "Diffuse"
}
