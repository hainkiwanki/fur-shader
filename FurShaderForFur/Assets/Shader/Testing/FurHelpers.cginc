#ifndef FUR_HELPERS_INCLUDED
#define FUR_HELPERS_INCLUDED

#include "UnityCG.cginc"
#include "Lighting.cginc"

struct vs_data
{
	float3 pos: POSITION;
	float3 norm: NORMAL;
	float2 texc: TEXCOORD0;
};

struct gs_data
{
	float4 pos : SV_POSITION;
	float3 norm : NORMAL;
	float2 texc : TEXCOORD0;
    float2 texc2 : TEXCOORD1;
};

struct gs_data_shell
{
    float4 pos : SV_POSITION;
    float3 norm : NORMAL;
    float2 texc : TEXCOORD0;
    int layer : TEXCOORD1;
};

float _MaxArea = 0.0f;

float3 CalculateLambert(float3 pos, float3 normal, float3 diffuse) {
	float NdotL = max(0.0f, dot(normal, normalize(normal - _WorldSpaceLightPos0)));
	float hLambert = NdotL * 0.5 + 0.5;
	float3 LambertDiffuse = (NdotL * diffuse);
	return  LambertDiffuse * 1.0f * _LightColor0;
}

float3 CalculateSpecularPhong(float3 viewDirection, float3 normal, float2 texCoord)
{
	float3 specularColor = float3(1.0f, 1.0f, 1.0f);
	float3 reflected = reflect(normalize(_WorldSpaceLightPos0), normal);
	specularColor *= pow(saturate(dot(-viewDirection, reflected)), 15);
	return specularColor;
}
#endif
