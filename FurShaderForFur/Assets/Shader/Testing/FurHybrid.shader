﻿Shader "Unlit/FurHybrid"
{
	Properties
	{
		_MainTex("Diffuse Texture", 2D) = "white" {}
		_RandomTex("Noise Texture", 2d) = "white" {}
		_MaxArea("Area", Range(0, 200)) = 200
		_BezierHeight("Height Bezier", Range(0.0, 20.0)) = 0.1
		_BezierCruve("Cruve Bezier", Range(0.0, 20.0)) = 0.1
		_HairLength("Max Length", Range(0.0, 1.0)) = 1.0
	}
		SubShader
		{
			Tags { "RenderType" = "Transparent" }
			LOD 100
			CGPROGRAM
			#pragma surface surf Lambert
			struct Input {
				float2 uv_MainTex;
			};
			sampler2D _MainTex;
			void surf(Input IN, inout SurfaceOutput o) {
				o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			}
			ENDCG

			Pass{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			CGPROGRAM
				#pragma enable_d3d11_debug_symbols
				#pragma vertex vert
				#pragma geometry geom
				#pragma fragment frag
				#define OFFSET 0
				#include "HybridHelpers.cginc"
			ENDCG
			}
			Pass{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			CGPROGRAM
				#pragma enable_d3d11_debug_symbols
				#pragma vertex vert
				#pragma geometry geom
				#pragma fragment frag
				#define OFFSET 1
				#include "HybridHelpers.cginc"
			ENDCG
			}
			Pass{
			Blend SrcAlpha OneMinusSrcAlpha
			Cull Off
			CGPROGRAM
				#pragma enable_d3d11_debug_symbols
				#pragma vertex vert
				#pragma geometry geom
				#pragma fragment frag
				#define OFFSET 2
				#include "HybridHelpers.cginc"
			ENDCG
			}
		}
}
