﻿Shader "GW/Shells/1_Layer" {
	Properties{
		//Skin
		_SkinTex("Skin Texture", 2D) = "white"{}
		_SkinNormalTex("Skin Normal", 2D) = "bump"{}
		_SkinColor("Skin Diffuse", Color) = (1,1,1,1)
		//Fur
		_FurTex("Fur Texture", 2D) = "white"{}
		_FurColor("Fur Diffuse", Color) = (1,1,1,1)
		_HairRootMap("Hair Noise Map", 2D) = "white" {}
		_HairControlMap("Height Map", 2D) = "white" {}
		_MaxFurLength("Fur Length", Float) = 0.4
		_FurThinness("Fur Thinness", Range(0.0, 10.0)) = 4.0
		_MinColor("Inter fur color", Range(0.0, 0.9)) = 0
		_MaxColor("Fur Darkness", Range(0.0, 0.9)) = 0.9
		_AlphaCutout("Max Alpha Value", Range(0.0, 1.0)) = 0.59
	}
	SubShader{
		Tags { "Queue" = "Transparent" "RenderType" = "Opaque" "IgnoreProjector" = "True"}
		ZWrite On
		Blend Off

		CGPROGRAM
			#pragma surface surfMain BlinnPhong addshadow vertex:vertMain
			#define AMOUNTLAYERS 1
			#define CURRENTLAYER 0.0
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#include "Shell_Variables.cginc"
			void vertShell(inout appdata_full input, out Input output)
			{
				UNITY_INITIALIZE_OUTPUT(Input, output);

				//Length of hair
				float length = _MaxFurLength / 100.0f;

				//Extrusion
				input.vertex.xyz += input.normal * length;
			}

		void surfShell(Input input, inout SurfaceOutput output)
		{
			float3 noise = tex2D(_HairRootMap, input.uv_FurTex * _FurThinness).rgb;
			float a = noise.y;

			float3 diffuse;
			float height = tex2D(_HairControlMap, input.uv_FurTex).g;
			float2 pixel = input.uv_FurTex;
			float alpha = 0.0f;

			diffuse = float3(1.0f, 1.0f, 1.0f);
			clip(height > 0.0f ? 1 : -1);
			if (a >= 0.2f)
			{
				alpha = 0.9f;
			}
			float min = clamp(noise.b, _MinColor, _MaxColor);
			float max = clamp(noise.b, min + 0.1f, 1.0f);
			diffuse *= tex2D(_FurTex, pixel).rgb * _FurColor.rgb;
			output.Albedo = diffuse * alpha;
			output.Alpha = alpha;
			output.Emission = fixed3(0.0f, 0.0f, 0.0f);
			output.Specular = 1.0f;
			output.Gloss = 1.0f;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
