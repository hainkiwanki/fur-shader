//--------------------------------------
//Input Struct
//--------------------------------------
struct Input
{
    float2 uv_SkinTex;
    float2 uv_SkinNormalTex;
    float2 uv_FurTex;
};
//--------------------------------------
//Inspector Variables
//--------------------------------------
//Skin
sampler2D _SkinTex;
sampler2D _SkinNormalTex;
float4 _SkinColor;
//Fur
sampler2D _FurTex;
float4 _FurColor;
sampler2D _HairRootMap;
sampler2D _HairControlMap;
float _MaxFurLength;
float _FurThinness;
float _MinColor;
float _MaxColor;
float _AlphaCutout;
//Curling
float _CurlFrequency;
float _CurlSize;
float _CurlRandom;
//Clumping
sampler2D _WetnessTex;
float4 _WetColor;
float _ClumpMult;
float _WetnessFactor;
//Forces
int _Stiffness;
float _WindAmp;
float _WindMultiplier;
//--------------------------------------
//From Script Variables
//--------------------------------------
float3 _Force;
float _UseWind;
