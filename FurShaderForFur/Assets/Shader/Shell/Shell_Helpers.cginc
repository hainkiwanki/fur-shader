float rand(float n)
{
    return frac(sin(n) * 43758.5453123);
}

float lengthOfVector(float3 vec)
{
    return sqrt(pow(vec.x, 2) + pow(vec.y, 2) + pow(vec.z, 2));
}