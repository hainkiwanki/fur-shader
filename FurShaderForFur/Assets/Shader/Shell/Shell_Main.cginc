#include "Shell_Variables.cginc"
#include "Shell_Helpers.cginc"

void vertMain(inout appdata_full input, out Input output)
{
    UNITY_INITIALIZE_OUTPUT(Input, output);
}

void surfMain(Input input, inout SurfaceOutput output)
{
    output.Albedo = tex2D(_SkinTex, input.uv_SkinTex) * _SkinColor;
    output.Normal = UnpackNormal(tex2D(_SkinNormalTex, input.uv_SkinNormalTex));
    output.Specular = 15.0f;
    output.Gloss = 1.0f;
    output.Alpha = 1.0f;
}

void vertShell(inout appdata_full input, out Input output)
{
    UNITY_INITIALIZE_OUTPUT(Input, output);

    //Length of hair
    float length = CURRENTLAYER * (_MaxFurLength / AMOUNTLAYERS);
    float movementFactor = pow(CURRENTLAYER, _Stiffness);

    //Gravity
    float3 gravity = mul(unity_WorldToObject, clamp(_Force.xyz, -1, 1)).xyz;

    //Wind
    float3 windOffset = float3(0.0f, 0.0f, 0.0f);
    if (_UseWind > 0.5f)
    {
        float phase = _Time * _WindAmp;
        //float offset = (input.vertex.x + (input.vertex.z * 0.2)) * 0.5f;
        float offset = input.vertex.x;
        windOffset = float3(0, _WindMultiplier * sin(phase + offset) * _UseWind, 0);

    }
    windOffset = mul(unity_WorldToObject, windOffset.xyz).xyz;


    //Curling of hair
    float3 W = input.normal;
    float3 U = cross(W, float3(1, 0, 0));
    float3 V = cross(W, U);
    float b = 0.5f;
    float rot = CURRENTLAYER;
    float rot2 = CURRENTLAYER;
    if (_CurlRandom <= 0.5f)
    {
        rot += rand(input.vertex.x);
        rot2 += rand(input.vertex.y);
    }
    float3 normal = _CurlSize * cos(rot * _CurlFrequency) * U + _CurlSize * sin(rot2 * _CurlFrequency) * V + _CurlSize * b * W;
    
    //Extrusion
    input.vertex.xyz += (normalize(normal + input.normal) + ((gravity + windOffset) * movementFactor)) * length;
}

void surfShell(Input input, inout SurfaceOutput output)
{
    float3 noise = tex2D(_HairRootMap, input.uv_FurTex * _FurThinness).rgb;
    float a = noise.y;

    float3 diffuse;
    float4 wetness = tex2D(_WetnessTex, input.uv_FurTex);
    float height = tex2D(_HairControlMap, input.uv_FurTex).g;
    float2 pixel = input.uv_FurTex;
    float alpha = 0.0f;

    if (wetness.b > 0.0f)
    {
        float2 d = (((1 - _WetnessFactor) * CURRENTLAYER) + (_WetnessFactor * (1 + (pow(-log(40), -1) * log(CURRENTLAYER))))) * (input.uv_SkinTex - wetness.rg).xy * _ClumpMult * wetness.b;

        pixel += d;
        pixel = clamp(pixel, 0.0f, 1.0f);

        if (a >= 0.2f)
        {
            float wetness1 = tex2D(_WetnessTex, pixel).b;
            clip(wetness1 > 0.0f ? 1 : -1);
            if (wetness1 <= 0.001f || height < CURRENTLAYER)
                alpha = 0.0f;
            else
                alpha = 1.0f - clamp(CURRENTLAYER + (10.0 / 20.0), 0.3f, _AlphaCutout);
        }
        diffuse = _WetColor.rgb * clamp(CURRENTLAYER, 0.5f, 1.0f);
    }
    else
    {
        diffuse = float3(1.0f, 1.0f, 1.0f);
        if (height < CURRENTLAYER)
        {
            alpha = 0.0f;
        }
        else if (a >= 0.2f)
        {
            alpha = 1.0f - clamp(CURRENTLAYER + (10.0 / 20.0), 0.3f, _AlphaCutout);
        }
    }
    float min = clamp(noise.b, _MinColor, _MaxColor);
    float max = clamp(noise.b, min + 0.1f, 1.0f);
    diffuse *= tex2D(_FurTex, pixel).rgb * _FurColor.rgb * lerp(min, max, CURRENTLAYER);
    output.Albedo = diffuse; 
    output.Alpha = alpha;


    output.Emission = fixed3(0.0f, 0.0f, 0.0f);
    output.Specular = 1.0f;
    output.Gloss = 1.0f;
}