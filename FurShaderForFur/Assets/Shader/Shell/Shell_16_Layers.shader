﻿Shader "GW/Shells/16_Layers" {
	Properties{
		//Skin
		_SkinTex("Skin Texture", 2D) = "white"{}
		_SkinNormalTex("Skin Normal", 2D) = "bump"{}
		_SkinColor("Skin Diffuse", Color) = (1,1,1,1)
		//Fur
		_FurTex("Fur Texture", 2D) = "white"{}
		_FurColor("Fur Diffuse", Color) = (1,1,1,1)
		_HairRootMap("Hair Noise Map", 2D) = "white" {}
		_HairControlMap("Height Map", 2D) = "white" {}
		_MaxFurLength("Fur Length", Float) = 1.0
		_FurThinness("Fur Thinness", Range(0.0, 10.0)) = 4.0
		_MinColor("Inter fur color", Range(0.0, 0.9)) = 0.0
		_MaxColor("Fur Darkness", Range(0.0, 0.9)) = 0.9
		_AlphaCutout("Max Alpha Value", Range(0.0, 1.0)) = 0.59
		//Curling
		_CurlFrequency("Curl frequency", Float) = 0.0
		_CurlSize("Curl Size", Float) = 0.0
		_CurlRandom("Randomize Curls", Range(0.0, 1.0)) = 0.0
		//Clumping
		_WetnessTex("Wetness Texture", 2D) = "black" {}
		_WetColor("Wet Color", Color) = (0.6, 0.6, 0.6, 1)
		_ClumpMult("Clump Multiplier", Float) = 1.0
		_WetnessFactor("Wetness Factor", Range(-2.0, 0.0)) = 0.0
		//Forces
		_Stiffness("Fur Stiffness", Range(0, 3)) = 1
		_WindAmp("Wind Strength", Range(0.0, 200.0)) = 1.00
		_WindMultiplier("Wind Multiplier", Range(0.0, 2.0)) = 1.0
	}
	SubShader{
		Tags { "Queue" = "Transparent" "RenderType" = "Opaque" "IgnoreProjector" = "True"}
		ZWrite On
		Blend Off

		CGPROGRAM
			#pragma surface surfMain BlinnPhong addshadow vertex:vertMain
			#define AMOUNTLAYERS 1
			#define CURRENTLAYER 0.0
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 1.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 2.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 3.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 4.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 5.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 6.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 7.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 8.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 9.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 10.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 11.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 12.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 13.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 14.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 15.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG

		CGPROGRAM
			#pragma surface surfShell BlinnPhong addshadow vertex:vertShell alpha:blend
			#define AMOUNTLAYERS 16.0
			#define CURRENTLAYER 16.0 / AMOUNTLAYERS
			#include "Shell_Main.cginc"
		ENDCG
	}
	FallBack "Diffuse"
}
