#include "UnityCG.cginc"
#include "Hybrid_Helpers.cginc"

gs_data vert(vs_data input)
{
    gs_data output;
    output.pos = input.pos;
    output.norm = input.norm;
    output.texc = input.texc;
    return output;
}

[maxvertexcount(113)]
void geom(triangle gs_data vertices[3], inout TriangleStream<gs_data> triStream, uint id : SV_PrimitiveID)
{
    float triangleArea = SurfaceArea(vertices[0].pos, vertices[1].pos, vertices[2].pos);
    float areaRatio = triangleArea / _MaxArea;

    float3 avgN = (vertices[0].norm + vertices[1].norm + vertices[2].norm) / 3.0;
    float3 AB = vertices[1].pos - vertices[0].pos;
    float3 AC = vertices[2].pos - vertices[0].pos;
    int hairs = clamp(16 * areaRatio, 0, 16);

    _PrvRand = abs(Rand(id));

    for (int i = 0; i < hairs; i++)
    {
        float r1 = Rand(_PrvRand + OFFSET);
        float r2 = Rand(r1);
        float r3 = 1 - r1 - r2;
        if ((r1 + r2) > 1.0f)
        {
            r1 = 1 - r1;
            r2 = 1 - r2;
            r3 = 1 - r1 - r2;
        }
        _PrvRand = r1;

        float3 root = vertices[0].pos + r1 * AB + r2 * AC;
        float2 uv = vertices[0].texc * r3 + vertices[1].texc * r1 + vertices[2].texc * r2;
        //uv = vertices[0].texc;

        //float4 rootB = mul(unity_ObjectToWorld, float4(root - vertices[1].pos, 1));
        //float4 rootC = mul(unity_ObjectToWorld, float4(root - vertices[2].pos, 1));
        //float4 rootA = mul(unity_ObjectToWorld, float4(root - vertices[0].pos, 1));
        //
        //float2 uv = vertices[0].texc * length(normalize(rootA)) + vertices[1].texc * length(normalize(rootB)) + vertices[2].texc * length(normalize(rootC));
        //
        //float p1 = vertices[0].pos;
        //float p2 = vertices[1].pos;
        //float p3 = vertices[2].pos;
        //
        //float3 f1 = p1 - root;
        //float3 f2 = p2 - root;
        //float3 f3 = p3 - root;
        //
        //float a = length(cross(p1 - p2, p1 - p3));
        //float a1 = length(cross(f2, f3)) / a;
        //float a2 = length(cross(f3, f1)) / a;
        //float a3 = length(cross(f1, f2)) / a;
        //
        //float2 uv1 = vertices[0].texc;
        //float2 uv2 = vertices[1].texc;
        //float2 uv3 = vertices[2].texc;
        //
        //uv = uv1 * a1 + uv2 * a2 + uv3 * a3;

		//HAIR CREATING
        float3 v[7];
        float hairWidth = 0.002f;
        float hairLength = _BezierHeight / 10.0f;
		
        float3 pBC1[3];
        pBC1[0] = float3(root.x + hairWidth / 2.0f, root.yz);
        pBC1[1] = pBC1[0] + avgN * hairLength;
        float3 crossProduct = cross(float3(0, -1, 0), pBC1[0]);
        float3 crossProduct2 = cross(pBC1[0], crossProduct);
        pBC1[2] = pBC1[0] + crossProduct2 * _BezierCruve;
		
        float3 pBC2[2];
        pBC2[0] = float3(root.x - hairWidth / 2.0f, root.yz);
        pBC2[1] = pBC2[0] + avgN * hairLength;

        v[0] = float3(root.x + hairWidth, root.yz);
        v[1] = float3(root.x - hairWidth, root.yz);
        v[2] = BezierCurve(pBC1[0], pBC1[1], pBC1[2], _GuardHairLength * 0.2f);
        v[3] = BezierCurve(pBC2[0], pBC2[1], pBC1[2], _GuardHairLength * 0.2f);
        v[4] = BezierCurve(pBC1[0], pBC1[1], pBC1[2], _GuardHairLength * 0.4f);
        v[5] = BezierCurve(pBC2[0], pBC2[1], pBC1[2], _GuardHairLength * 0.4f);
        v[6] = BezierCurve(pBC1[0], pBC1[1], pBC1[2], _GuardHairLength * 0.6f);
        
        CreateVertex(triStream, float4(v[0], 1), avgN, uv);
        CreateVertex(triStream, float4(v[1], 1), avgN, uv);
        CreateVertex(triStream, float4(v[2], 1), avgN, uv);
        CreateVertex(triStream, float4(v[3], 1), avgN, uv);
        CreateVertex(triStream, float4(v[4], 1), avgN, uv);
        CreateVertex(triStream, float4(v[5], 1), avgN, uv);
        CreateVertex(triStream, float4(v[6], 1), avgN, uv);
        triStream.RestartStrip();
    }
}

float4 frag(gs_data input) : SV_TARGET
{
    float3 diffuse = tex2D(_MainTex, input.texc);

    return float4(diffuse, 1);
}