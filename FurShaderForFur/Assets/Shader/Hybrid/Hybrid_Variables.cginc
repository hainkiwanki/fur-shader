#include "UnityCG.cginc"
struct vs_data
{
    float4 pos : POSITION;
    fixed3 norm : NORMAL;
    fixed2 texc : TEXCOORD0;
};

struct gs_data
{
    float4 pos : SV_POSITION;
    fixed3 norm : NORMAL;
    fixed2 texc : TEXCOORD0;
};

struct Input
{
    float2 uv_MainTex;
};

sampler2D _MainTex;
sampler2D _RandomTex;
float _MaxArea;
float _PrvRand;
float _BezierHeight;
float _BezierCruve;
float _GuardHairLength;
float _UnderCoatLength;