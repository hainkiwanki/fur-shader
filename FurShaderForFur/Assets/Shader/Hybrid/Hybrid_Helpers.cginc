#include "UnityCG.cginc"
#include "Hybrid_Variables.cginc"

float Rand(float n) {
    return frac(sin(n) * 43758.5453123);
}

float SurfaceArea(float3 A, float3 B, float3 C)
{
    float3 AC = C - A;
    float3 AB = B - A;
    float3 BC = C - B;

    float a = length(AB);
    float b = length(AC);
    float c = length(BC);
    float s = (a + b + c) / 2.0;
    float Area = sqrt(s * (s - a) * (s - b) * (s - c));

    return Area;
}

void CreateVertex(inout TriangleStream<gs_data> triStream, float4 pos, float3 normal, float2 texCoord)
{
    gs_data gsData = (gs_data) 0;
    gsData.pos = UnityObjectToClipPos(pos);
    gsData.norm = mul(normal, (float3x3) unity_WorldToObject);
    gsData.texc = texCoord;
    triStream.Append(gsData);
}

float3 BezierCurve(float3 p1, float3 p2, float3 p3, float t)
{
    float3 P = 0;
    float3 a = pow((1 - t), 2) * p1;
    float3 b = 2 * (1 - t) * t * p2;
    float3 c = pow(t, 2) * p3;
    P = a + b + c;
    return P;
}
