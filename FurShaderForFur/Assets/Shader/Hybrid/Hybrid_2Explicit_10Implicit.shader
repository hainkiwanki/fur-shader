﻿Shader "GW/Hybrid/2Explicit_10Implicit"
{
	Properties
	{
		_MainTex("Diffuse Texture", 2D) = "white" {}
		_RandomTex("Noise Texture", 2d) = "white" {}
		_MaxArea("Area", Range(0.001, 0.1)) = 0.01
		_BezierHeight("Height Bezier", Range(0.0, 10.0)) = 0.1
		_BezierCruve("Cruve Bezier", Range(0.0, 10.0)) = 0.1
		_GuardHairLength("Max Guard Hair Length", Range(0.0, 1.0)) = 1.0
		_UnderCoatLength("Max Undercoat Length", Range(0.0, 1.0)) = 1.0
	}
		SubShader
		{
			Tags { "RenderType" = "Transparent" "RenderType" = "Opaque" "IgnoreProjector" = "True" }
			LOD 100

			//Render original mesh
			CGPROGRAM
				#pragma surface surf Lambert
				struct Input {
					float2 uv_MainTex;
				};
				sampler2D _MainTex;
				void surf(Input IN, inout SurfaceOutput o) {
					o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
				}
			ENDCG

				//Explicit Hairs
				Pass
				{
					Blend SrcAlpha OneMinusSrcAlpha
					Cull Off
					CGPROGRAM
						#pragma enable_d3d11_debug_symbols
						#pragma vertex vert
						#pragma geometry geom
						#pragma fragment frag
						#define OFFSET 0
						#include "Hybrid_Explicit.cginc"
					ENDCG
				}
				Pass
				{
					Blend SrcAlpha OneMinusSrcAlpha
					Cull Off
					CGPROGRAM
						#pragma enable_d3d11_debug_symbols
						#pragma vertex vert
						#pragma geometry geom
						#pragma fragment frag
						#define OFFSET 1
						#include "Hybrid_Explicit.cginc"
					ENDCG
				}
				//Implicit Hairs
				ZWrite On
				Blend Off
				CGPROGRAM
					#pragma surface surfSkin BlinnPhong vertex:vertSkin
					#define CURRENTLAYER 0.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 1.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 2.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 3.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 4.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 5.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 6.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 7.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 8.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 9.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

				CGPROGRAM
					#pragma surface surf BlinnPhong vertex:vert alpha:blend
					#define CURRENTLAYER 10.0 / 10.0
					#include "Hybrid_Implicit.cginc"
				ENDCG

		}
}
