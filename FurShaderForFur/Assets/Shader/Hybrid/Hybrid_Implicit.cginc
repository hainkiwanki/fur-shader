#include "UnityCG.cginc"
#include "Hybrid_Variables.cginc"

void vertSkin(inout appdata_full input, out Input output)
{
    UNITY_INITIALIZE_OUTPUT(Input, output);
}

void surfSkin(Input input, inout SurfaceOutput output)
{
    output.Albedo = tex2D(_MainTex, input.uv_MainTex);
}

void vert(inout appdata_full input)
{
    float length = CURRENTLAYER * (_UnderCoatLength / 10.0f);
    input.vertex.xyz += input.normal * length;
}

void surf(Input input, inout SurfaceOutput output)
{
    float4 d = tex2D(_MainTex, input.uv_MainTex);
    float4 a = tex2D(_RandomTex, input.uv_MainTex).r;
    output.Albedo = d.rgb * lerp(0, 1, CURRENTLAYER);
    output.Alpha = 1.0f - clamp(CURRENTLAYER, 0.3f, 1.0f);
}